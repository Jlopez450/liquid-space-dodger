playmusic:
		cmpi.w #$0000,timer
		bne music_driver
		sub.w #$0001,timer	
		rts
music_driver:
		clr d0
		bsr test2612
		
        move.b (a1)+,d0
		cmpi.b #$61,d0
		 beq wait		
	
		 cmpi.b #$66,d0
		 beq loop_playback
		
		cmpi.b #$52,d0 
		 beq update2612_0
		cmpi.b #$53,d0 
		 beq update2612_1
		cmpi.b #$50,d0
		 beq update_psg
		bra music_driver
	
update2612_0:
        move.b (a1)+,$A04000
		nop
        move.b (a1)+,$A04001
        bra music_driver
		
update2612_1:	
	    move.b (a1)+,$A04002
		nop
        move.b (a1)+,$A04003
		bra music_driver
		
loop_playback:
		add.b #$01,musicnum
		cmpi.b #$07,musicnum
		bge resetmus
		bsr newmusic
		add.w #$01,speed		
 		move.b #$9f,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011
		bra music_driver
resetmus:
		move.b #$00,musicnum
		bra loop_playback
	
update_psg:
        move.b (a1)+,$C00011
		bra music_driver
	
wait:
		move.b #$ff,running		
		clr d0
		clr d1
		move.b (a1)+,d1
		move.b (a1)+,d0			
		lsl.w #$08,d0    ;switch to big endian
		eor.w d0,d1      ;ditto
		;lsl.w #$01,d1     ;tempo adjust (removed in favor of more NOPs)
		move.w d1,timer
		rts

test2612:
		clr d0
        move.b $A04001,d0
		andi.b #$80,d0
        cmpi.b #$80,d0
		beq test2612 
		rts	
		
kill_2612:
		clr d0
		move.w #$0030,d4
killloop:		
        move.b d0,$A04000
		nop
        move.b #$00,$A04001	
		add.b #$01,d0
		dbf d4,killloop
		rts	
newmusic:
		clr d6
		tst musicID
		bne newmusicB
		move.b musicnum,d6
		lea (musictable),a0
        lsl.b #$1,d6		    ;locate the correct table
		sub.w #$02,d6           ;step back a word 
		add.w d6,a0             ;adjust the address register 
		move.w (a0),d6
		move.l d6,a0            ;switch to direct addressing 	
		jmp (a0)	
		
newmusicB:
		move.b musicnum,d6
		lea (musictableB),a0
        lsl.b #$1,d6		    ;locate the correct table
		sub.w #$02,d6           ;step back a word 
		add.w d6,a0             ;adjust the address register 
		move.w (a0),d6
		move.l d6,a0            ;switch to direct addressing 	
		jmp (a0)		

musictable:
 dc.w ld_music1
 dc.w ld_music2
 dc.w ld_music3
 dc.w ld_music4
 dc.w ld_music5 
 dc.w ld_music6

ld_music1: 
		lea (music1)+40,a1
		move.l a1,vgm_start
		rts
ld_music2: 
		lea (music2)+40,a1
		move.l a1,vgm_start
		rts
ld_music3: 
		lea (music3)+40,a1
		move.l a1,vgm_start
		rts
ld_music4: 
		lea (music4)+40,a1
		move.l a1,vgm_start
		rts
ld_music5: 
		lea (music5)+40,a1
		move.l a1,vgm_start
		rts
ld_music6: 
 		lea (music6)+40,a1
		move.l a1,vgm_start
		rts

musictableB:
 dc.w ld_music1B ;original
 dc.w ld_music2B ;fantasy zone
 dc.w ld_music6B ;turrican 
 dc.w ld_music5B ;genesis
 dc.w ld_music4B ;time trax 
 dc.w ld_music3B ;kid chameleon
 
ld_music1B: 
		lea (music1B)+40,a1
		move.l a1,vgm_start
		rts
ld_music2B: 
		lea (music2B)+40,a1
		move.l a1,vgm_start
		rts
ld_music3B: 
		lea (music3B)+40,a1
		move.l a1,vgm_start
		rts
ld_music4B: 
		lea (music4B)+40,a1
		move.l a1,vgm_start
		rts
ld_music5B: 
		lea (music5B)+40,a1
		move.l a1,vgm_start
		rts
ld_music6B: 
 		lea (music6B)+40,a1
		move.l a1,vgm_start
		rts 
              