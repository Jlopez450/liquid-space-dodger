	include "Music_driver.asm"
		
VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ; Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8406	;field B	
	dc.w $857c	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8A00		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3D		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200

paleyer_palette:
	dc.w $0000,$0000,$00EE,$0EEE,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
background_palette:
	dc.w $0000,$008E,$02EE,$006E,$000E,$0E22,$0E2A,$062C
	dc.w $0088,$02E4,$0606,$0260,$0EC2,$0EEE,$0066,$0688
palette_spike:
	dc.w $0000,$0EEE,$0000,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
	
title_GFX:
	dc.b "                                        "
	dc.b "          Liquid Space Dodger!          "
	dc.b "     Press START or click to begin      "
	dc.b "                                        "
	dc.b " How to play:                           "
	dc.b " Use the D-pad to steer left and right  "
	dc.b " to avoid the spikes.                   "
	dc.b "                                        "
	dc.b " Use port 1 for pad & port 2 for mouse! "
	dc.b " When using a controller, you can hold  "
	dc.b " A, B, or C to toggle between slow,     "
	dc.b " medium, and fast movement speed.       "
	dc.b "                                        "
	dc.b " The spikes will fall faster and faster "
	dc.b " after each music change. Be ready!     "
	dc.b " You can survive 4 hits before dying.   "
	dc.b "                                        "
	dc.b "  Music A (Normal)<--                   "
	dc.b "    Music B (Hard)                      "
	dc.b "                                        "
	dc.b "  Credits:                              "
	dc.b "            'A' music by Hideyuki Ogura "		
	dc.b "               'B' music by James Lopez "	
	dc.b "              Programmed by James Lopez "
	dc.b "                Graphics by James Lopez "	
	dc.b "                                        "	
	dc.b "                      ",$7f,"2015 James Lopez "	
	dc.b "                                       %"	

	
hud:
	dc.b "                                                                "
	dc.b "  Time - XX:XX  Speed - XX  Level - XX       $"
	
end_text:
	;dc.b "  GAME OVER! Press START to try again!   $"	
	dc.b "               GAME OVER!                $"
end_text2:	
	dc.b "   Click or press START to try again!    $"
	
	
font:
	incbin "gfx/ascii.bin"
	incbin "gfx/hexascii.bin"
	
playersprite:
	incbin "gfx/playersprites.bin"

spike:
	incbin "gfx/spike.bin"
	
background:
	incbin "gfx/background.bin"
	
map:
	incbin "gfx/bg_map.bin"
		
music1:
	incbin "music/music1.vgm"
music2:
	incbin "music/music2.vgm"
music3:
	incbin "music/music3.vgm"
music4:
	incbin "music/music4.vgm"
music5:
	incbin "music/music5.vgm"
music6:
	incbin "music/music6.vgm"	
music1b:
	incbin "music/music1b.vgm"
music2b:
	incbin "music/music2b.vgm"
music3b:
	incbin "music/music3b.vgm"
music4b:
	incbin "music/music4b.vgm"
music5b:
	incbin "music/music5b.vgm"
music6b:
	incbin "music/music6b.vgm"	
mute:
	incbin "music/mute.vgm"
	
sine:
	incbin "sine.bin"
	incbin "sine.bin"
