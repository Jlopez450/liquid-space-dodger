    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
 		move.w #$100,($A11100)
		move.w #$100,($A11200) 
		move.l #$FFFFF0,a7 		
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		lea (font),a5
		move.w #$0BFF,d4
		move.l #$40000000,(a3)
		bsr vram_loop
		
		lea (paleyer_palette),a5
		move.l #$c0000000,(a3)
		move.w #$002F,d4
		bsr vram_loop		
		
		bsr titlescreen
		
		lea (playersprite),a5
		move.l #$5C000000,(a3)
		move.w #$11FF,d4
		bsr vram_loop
		
		lea (spike),a5
		move.w #$0100,d4
		move.l #$58000000,(a3)
		bsr vram_loop
		
		lea (background),a5
		move.w #$24d0,d4
		move.l #$40000001,(a3)
		bsr vram_loop
		
		lea (map),a5
		move.w #$07FF,d4
		move.l #$40000003,(a3)
		bsr map_background
		
		lea (hud),a5
		clr d4
		move.l #$60000002,(a3)
		bsr termtextloop
				
		move.b #$01,musicnum
		bsr newmusic
		
		move.w #$0110,xpos
		move.w #$00EE,lives
		move.b #$05,removed
		move.b #$ff,hitflag	 ;fixes color bug on reset. Causes a bug that lets you hit the first barrier.
		move.b #$01,musicnum ;who the hell is going to crash into the first obstacle anyway?
		move.w #$0001,speed
		bsr region_check
        move.w #$2300, sr       ;enable ints		
		
loop:
		move.w #$0000,freetime
		bsr read_controller	
		bsr test_input	
		bsr read_mouse	
		bsr mouse_input
		bsr animate_sprites	
		bsr test_collision
		bsr playmusic		
		move.b #$ff,vb_flag	
vb_wait:
		add.w #$0001,freetime		
		bsr random_number				
		;cmpi.b #$00,vb_flag
		;bne vb_wait
		tst vb_flag
		bne vb_wait
		bra loop
		
remove_random:
		move.b random,d6
		; andi.w #$000F,d6
		; lsr.w #$01,d6
		; add.w #$01,d6
		; cmpi.b #$00,d6
		; beq force
		move.b d6,removed	
		rts
force:
	    move.b #$01,d6
	    move.b d6, removed
		rts
mouse_input:
		move.l mousedat,d0
		cmpi.w #$0000,d0
		beq return ;no input
		swap d0
		andi.b #$10,d0
		cmpi.b #$10,d0
		beq mouseleft
		bra mouseright
		
mouseleft:
		cmpi.w #$0080,xpos
		ble return
		move.w mousedat,d0	
		swap d0
		eor.w #$ffff,d0 ;deal with it
		lsr.w #$08,d0
		;lsr.w #$04,d0
		sub.w d0,xpos			
		rts
mouseright:
		cmpi.w #$01a0,xpos
		bge return
		move.w mousedat,d0
		swap d0
		andi.w #$ff00,d0
		lsr.w #$08,d0		
		add.w d0,xpos	
		rts		
		
test_input:
		move.w #$0002,playerspeed			
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7
		beq slow
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7
		beq medium
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7
		beq fast	
speedret:		
		move.b d3,d7
		or.b #$FB,d7
		cmpi.b #$FB,d7
		beq left
		
		move.b d3,d7
		or.b #$F7,d7
		cmpi.b #$F7,d7
		beq right
		
		rts
		
slow:
		move.w #$0004,playerspeed
		bra speedret
medium:
		move.w #$0007,playerspeed
		bra speedret
fast:
		move.w #$0009,playerspeed
		bra speedret		
		
left:
		cmpi.w #$0080,xpos
		ble return
		move.w playerspeed,d7
		sub.w d7,xpos
		rts

right:
		cmpi.w #$01a0,xpos
		bge return
		move.w playerspeed,d7
		add.w d7,xpos
		rts
		
random_number:
		add.b #$01,random
		cmpi.b #$09,random
		bgt resetrandom
		rts
resetrandom:
		move.b #$00,random
		bra random_number
		
		; add.w d0,random
		; add.w d1,random
		; add.w d2,random
		; add.w d3,random
		; add.w d4,random
		; add.w d5,random
		; add.w d6,random
		; add.w d7,random
		; rts
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		move.b d3, input
		rts				
		
animate_sprites:
		bsr spin_player
		bsr animate_spikes
		rts
		
resetwall:
		move.w #$0000,wallpos
		move.b #$00,hitflag
		clr d6
		clr d5	
		move.b #$01, d5
		move.b levelnum, d6	
		abcd d5, d6
		move.b d6, levelnum 				
		;bsr remove_random	
		move.b random,removed
animate_spikes:
		move.w speed,d6
		add.w d6,wallpos
		cmpi.w #$0200,wallpos
		bge resetwall
		lea (spritebuffer)+8,a5
		move.w wallpos,(a5)+
		move.w #$0f02,(a5)+
		move.w #$40c0,(a5)+
		move.w #$0080,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f03,(a5)+
		move.w #$40c0,(a5)+
		move.w #$00A0,(a5)+		
		move.w wallpos,(a5)+
		move.w #$0f04,(a5)+
		move.w #$40c0,(a5)+
		move.w #$00c0,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f05,(a5)+
		move.w #$40c0,(a5)+
		move.w #$00e0,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f06,(a5)+
		move.w #$40c0,(a5)+
		move.w #$0100,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f07,(a5)+
		move.w #$40c0,(a5)+
		move.w #$0120,(a5)+		
		move.w wallpos,(a5)+
		move.w #$0f08,(a5)+
		move.w #$40c0,(a5)+
		move.w #$0140,(a5)+
		move.w wallpos,(a5)+
		move.w #$0f09,(a5)+
		move.w #$40c0,(a5)+
		move.w #$0160,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f0a,(a5)+
		move.w #$40c0,(a5)+
		move.w #$0180,(a5)+
		move.w wallpos,(a5)+
		move.w #$0f0b,(a5)+
		move.w #$40c0,(a5)+
		move.w #$01a0,(a5)+
		
		clr d6
		move.b removed,d6
		lea (removetable),a0
        lsl.b #$1,d6		    ;locate the correct table
		sub.w #$02,d6           ;step back a word 
		add.w d6,a0             ;adjust the address register 
		move.w (a0),d6
		move.l d6,a0            ;switch to direct addressing 	
		jmp (a0)
		
removetable:
 dc.w remove1
 dc.w remove2
 dc.w remove3
 dc.w remove4
 dc.w remove5
 dc.w remove6
 dc.w remove7
 dc.w remove8
 dc.w remove9
 
remove1:
		lea (spritebuffer)+8,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+16,a5
		move.w #$0000,(a5)		
		rts
remove2:
		lea (spritebuffer)+16,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+24,a5
		move.w #$0000,(a5)		
		rts		
remove3:	
		lea (spritebuffer)+24,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+32,a5
		move.w #$0000,(a5)		
		rts	
remove4:
		lea (spritebuffer)+32,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+40,a5
		move.w #$0000,(a5)		
		rts		
remove5:
		lea (spritebuffer)+40,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+48,a5
		move.w #$0000,(a5)		
		rts		
remove6:	
		lea (spritebuffer)+48,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+56,a5
		move.w #$0000,(a5)		
		rts		
remove7:
		lea (spritebuffer)+56,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+64,a5
		move.w #$0000,(a5)		
		rts				
remove8:
		lea (spritebuffer)+64,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+72,a5
		move.w #$0000,(a5)		
		rts				
remove9:
		lea (spritebuffer)+72,a5
		move.w #$0000,(a5)
		lea (spritebuffer)+80,a5
		move.w #$0000,(a5)		
		rts			
		
test_collision:		
		clr d6
		move.w (a3),d6
		andi.w #$0020,d6
		tst d6
		bne collide
		move.l #$c0000000,(a3)
		move.w #$0000,(a4)		
		rts		
		
collide:
		; cmpi.b #$ff,hitflag
		 ; beq return
		cmpi.w #$0120,wallpos ;hack to make the came work better on emulators
		ble return
		cmpi.w #$0160,wallpos ;another hack to make the came work better on emulators
		bge return		
		tst hitflag
		 bne return		 
		cmpi.w #$0022,lives
		 beq game_over
		sub.w #$0044,lives
		move.l #$c0040000,(a3)
		move.w lives,(a4)
		move.b #$ff,hitflag
		rts
game_over:
		move.w #$2700,sr
		lea (end_text),a5
		move.l #$68000002,(a3)
		bsr termtextloop_P
		lea (end_text2),a5
		move.l #$68800002,(a3)
		bsr termtextloop_P		
		lea (mute)+40,a1
		bsr music_driver
		;bsr kill_2612
game_over_loop:
		bsr GO_PAD
		bsr GO_MOUSE
		bra game_over_loop

GO_PAD:
		bsr read_controller
		or.b #$7f,d3
		cmpi.b #$7f,d3
		beq start
		rts
GO_MOUSE:
		bsr read_mouse
		tst.l mousedat ;no mouse?
		bmi return			
		move.l mousedat,d0
		swap d0
		andi.w #$0f,d0
		tst d0
		bne start  		
		rts
		
spin_player:
		eor.b #$ff,inverter
		tst inverter
		beq return
		add.b #$01,playerframe
		cmpi.b #$13,playerframe
		bge loopframe

		lea (playerframes),a0
		move.l #$00000000,d0
		move.b playerframe,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		lea (spritebuffer),a5		
		jmp (a0)
loopframe:
		move.b #$00,playerframe
		bra animate_sprites
		
playerframes:
 dc.w ld_frame1
 dc.w ld_frame2
 dc.w ld_frame3
 dc.w ld_frame4
 dc.w ld_frame5
 dc.w ld_frame6
 dc.w ld_frame7
 dc.w ld_frame8
 dc.w ld_frame9
 dc.w ld_frame10
 dc.w ld_frame11
 dc.w ld_frame12
 dc.w ld_frame13
 dc.w ld_frame14
 dc.w ld_frame15
 dc.w ld_frame16
 dc.w ld_frame17
 dc.w ld_frame18
	
ld_frame1:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$00E0,(a5)+
		move.w xpos,(a5)+
		rts
ld_frame2:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$00F0,(a5)+
		move.w xpos,(a5)+
		rts		
ld_frame3:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0100,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame4:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0110,(a5)+
		move.w xpos,(a5)+
		rts			
ld_frame5:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0120,(a5)+
		move.w xpos,(a5)+
		rts			
ld_frame6:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0130,(a5)+
		move.w xpos,(a5)+
		rts			
ld_frame7:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0140,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame8:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0150,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame9:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0160,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame10:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0170,(a5)+
		move.w xpos,(a5)+
		rts
ld_frame11:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0180,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame12:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$0190,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame13:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$01a0,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame14:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$01b0,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame15:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$01c0,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame16:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$01d0,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame17:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$01e0,(a5)+
		move.w xpos,(a5)+
		rts	
ld_frame18:
		move.w ypos,(a5)+
		move.w #$0F01,(a5)+
		move.w #$01f0,(a5)+
		move.w xpos,(a5)+
		rts			
		
termtextloop:					;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4			;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
        move.w d4,(a4)		
		bra termtextloop
termtextloop_p:
		move.b (a5)+,d4	
		cmpi.b #$24,d4
		beq return
		andi.w #$00ff,d4
		eor.w #$8000,d4		;add priority
        move.w d4,(a4)		
		bra termtextloop		
	
setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts  
clear_regs:
		move.l #$00000000,d0
		move.l #$00000000,d1
		move.l #$00000000,d2
		move.l #$00000000,d3
		move.l #$00000000,d4
		move.l #$00000000,d5
		move.l #$00000000,d6
		move.l #$00000000,d7
		move.l #$00000000,a0
		move.l #$00000000,a1
		move.l #$00000000,a2
		move.l #$00000000,a3
		move.l #$00000000,a4
		move.l #$00000000,a5
		move.l #$00000000,a6
		rts
region_check:
		clr d0
        move.b  $A10001, d0
		andi.b #$40, d0
		cmpi.b #$40, d0
		 beq pal 
		bra ntsc
pal:
		move.b #$50, region
		move.w #$817c,(a3)
		move.w #$014E,ypos			
		rts
ntsc:				
        move.b #$60, region
		move.w #$8174,(a3)	
		move.w #$013E,ypos		
		rts			
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts	
		
map_background:
		clr d7
map_backgroundloop:
		move.w (a5)+,d7	
		add.w #$2200,d7
		move.w d7,(a4)
		dbf d4,map_backgroundloop
		rts	

smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return	
		cmpi.b #$20,d5
		 beq clearspace			;band-aid fix
clear_return:		 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
clearspace:
		move.b #$00,d5
		bra clear_return	

titlescreen:
		lea (title_GFX),a5
		move.l #$60000002,(a3)
		bsr smalltextloop
titleloop:
		bsr titlecontrol	
		bsr read_mouse
		tst.l mousedat ;no mouse?
		bmi skipmouse			
		move.l mousedat,d0
		swap d0
		;btst #$01,d0
		andi.w #$0f,d0
		tst d0
		bne end_title  	
skipmouse:		
		bsr read_controller
		or.b #$7f,d3
		cmpi.b #$7f,d3
		bne titleloop
end_title:		
		move.l #$60000002,(a3)		
		move.w #$1000,d4
clear_title:
		move.w #$0000,(a4)
		dbf d4, clear_title
		rts
titlecontrol:
		bsr read_controller
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7	;up	
		beq setmusA
		move.b d3,d7		
		or.b #$fd,d7
		cmpi.b #$fd,d7	;down
		beq setmusB		
		rts
setmusA:
		move.l #$68A40002,(a3) ;vram A8A4
		move.w #$003c,(a4)
		move.w #$002d,(a4)
		move.w #$002d,(a4)
		move.l #$69240002,(a3) ;vram a924
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.b #$00,musicID
		rts
setmusB:
		move.l #$69240002,(a3) ;vram a924
		move.w #$003c,(a4)
		move.w #$002d,(a4)
		move.w #$002d,(a4)
		move.l #$68A40002,(a3) ;vram A8A4
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)		
		move.b #$ff,musicID	
		rts
		
HBlank:
		bsr warp		
        rte

VBlank:	
		movem.l d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a5/a6, -(sp)			
		;cmpi.b #$00,vb_flag
		;beq vb_end
		bsr calc_raster		
		bsr clock
		bsr update_sprites	
		bsr update_hud
		move.b #$00,vb_flag	
vb_end:		
		movem.l (sp)+, d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a5/a6		
        rte
		
calc_raster:
		add.w #$0002,vblanks
	    cmpi.w #$0200,vblanks
	    bge reset_vb	
		
	    lea (sine),a2
	    move.l a2,d7
	    add.w vblanks,d7
		;andi.w #$FFFE,d7		
	    move.l d7,a2
		rts
		
reset_vb:
		move.w #$0000,vblanks
		bra calc_raster
		
update_sprites:
		lea (spritebuffer),a6
		move.l #$78000003,(a3)
		move.w #$003F,d4
spriteloop:
		move.w (a6)+,(a4)
		dbf d4,spriteloop
		rts	

update_hud:
		move.l #$60B00002,(a3)
        move.w speed,d0
		bsr writebyte	
		
		move.l #$60C80002,(a3)
        move.b levelnum,d0
		bsr writebyte	

		move.l #$60920002,(a3)
        move.b minutes,d0
		bsr writebyte	

		move.l #$60980002,(a3)
        move.b seconds,d0
		bsr writebyte
		rts ;comment this out to enable debug HUD.
		
		move.l #$60020002,(a3)
        move.w freetime,d0
		bsr writeword		

		move.l #$600C0002,(a3)
        move.l mousedat,d0
		swap d0
		bsr writeword
		swap d0
		bsr writeword
		
		move.l #$601e0002,(a3)
        move.w wallpos,d0
		bsr writeword	

		move.l #$60280002,(a3)
        move.w xpos,d0
		bsr writeword	

		move.l #$60320002,(a3)
        move.b ticks,d0
		bsr writebyte

		move.l #$60380002,(a3)
        move.b hitflag,d0
		bsr writebyte	

		move.l #$603E0002,(a3)
        move.b input,d0
		bsr writebyte				
		rts
		
writeword:
	    move.w d0,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)
	    move.w d0,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)
writebyte:  ;not to be confused with litebrite				
	    move.w d0,d5
		andi.w #$00f0,d5
		lsr #$4,d5
		;andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)
	    move.w d0,d5
		andi.w #$000f,d5		
		;andi.w #$00ff, d5
    	add.w #$00b0,d5	
		move.w d5,(a4)		
		rts
		
return:
		rts
returnint:
		rte	
	
warp:	
	   move.w sr, -(sp)
	   move.w #$2700,sr
	   
	   move.l #$50000010,(a3) ;write to VSRAM		   
	   move.w (a2)+,d2
	   lsr.w #$02,d2
	   move.w #$0000,(a4)
	   move.w d2,(a4)		  ;write scroll data	
	   
	   lsr.w #$03,d2	   
	   move.l #$74000003,(a3) ;write to HSRAM
	   move.w #$0000,(a4)
	   move.w d2,(a4)		  ;write scroll data	   
	      
	   move.w (sp)+,sr	   
	   rts
	
	include "clock.asm"
	include "mousedriver.asm"
	include "crashscreen.asm"
	include "data.asm"
ROM_End:
              
              