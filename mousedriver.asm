read_mouse: 
	movem.l d0/d1/d2/a0, -(sp)
	moveq #$00000000,d0
	moveq #$00000000,d1
	moveq #$00000000,d2
	lea $a10005,a0		
	bsr get_mky
	move.l d0, mousedat
	movem.l (sp)+, d0/d1/d2/a0
	rts
get_mky:
        ;move.w  sr,d2
        ;move.w  #$2700,sr      ;/* disable ints */

        move.b	#$60,6(a0)     ;/* set direction bits */
        nop
        nop
        move.b  #$60,(a0)      ;/* first phase of mouse packet */
        nop
        nop
La0:
        btst    #4,(a0)
        beq.b   La0              ;/* wait on handshake */
        move.b  (a0),d0
        andi.b  #15,d0
        bne     mky_err         ;/* not 0 means not mouse */

        move.b  #$20,(a0)      ;/* next phase */
        move.w  #254,d1         ;/* number retries before timeout */
La1:
        btst    #4,(a0)
        bne.b   La2              ;/* handshake */
        dbra    d1,La1
        bra     timeout_err
La2:
        move.b  (a0),d0
        andi.b  #15,d0
        move.b  #0,(a0)         ;/* next phase */
        cmpi.b  #11,d0
        bne     mky_err         ;/* not 11 means not mouse */
La3:
        btst    #4,(a0)
        beq.b   La4               ;/* handshake */
        dbra    d1,La3
        bra     timeout_err
La4:
        move.b  (a0),d0         ;/* specs say should be 15 */
        nop
        nop
        move.b  #$20,(a0)      ;/* next phase */
        nop
        nop
La5:
        btst    #4,(a0)
        bne.b   La6
        dbra    d1,La5
        bra     timeout_err
La6:
        move.b  (a0),d0         ;/* specs say should be 15 */
        nop
        nop
        move.b  #0,(a0)         ;/* next phase */
        moveq   #0,d0           ;/* clear reg to hold packet */
        nop
La7:
        btst    #4,(a0)
        beq.b   La8               ;/* handshake */
        dbra    d1,La7
        bra     timeout_err
La8:
        move.b  (a0),d0         ;/* YO XO YS XS */
        move.b  #$20,(a0)      ;/* next phase */
        lsl.w   #8,d0           ;/* save nibble */
La9:
        btst    #4,(a0)
        bne.b   La10              ;/* handshake */
        dbra    d1,La9
        bra     timeout_err
La10:
        move.b  (a0),d0         ;/* S  M  R  L */
        move.b  #0,(a0)         ;/* next phase */
        lsl.b   #4,d0           ;/* YO XO YS XS S  M  R  L  0  0  0  0 */
        lsl.l   #4,d0           ;/* YO XO YS XS S  M  R  L  0  0  0  0  0  0  0  0 */
La11:
        btst    #4,(a0)
        beq.b   La12              ;/* handshake */
        dbra    d1,La11
        bra     timeout_err
La12:
        move.b  (a0),d0         ;/* X7 X6 X5 X4 */
        move.b  #$20,(a0)      ;/* next phase */
        lsl.b   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 0  0  0  0 */
        lsl.l   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 0  0  0  0  0  0  0  0 */
La13:
        btst    #4,(a0)
        bne.b   La14              ;/* handshake */
        dbra    d1,La13
        bra     timeout_err
La14:
        move.b  (a0),d0         ;/* X3 X2 X1 X0 */
        move.b  #0,(a0)         ;/* next phase */
        lsl.b   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 X3 X2 X1 X0 0  0  0  0 */
        lsl.l   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 X3 X2 X1 X0 0  0  0  0  0  0  0  0 */
La15:
        btst    #4,(a0)
        beq.b   La16              ;/* handshake */
        dbra    d1,La15
        bra     timeout_err
La16:
        move.b  (a0),d0         ;/* Y7 Y6 Y5 Y4 */
        move.b  #$20,(a0)      ;/* next phase */
        lsl.b   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 X3 X2 X1 X0 Y7 Y6 Y5 Y4 0  0  0  0 */
        lsl.l   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 X3 X2 X1 X0 Y7 Y6 Y5 Y4 0  0  0  0  0  0  0  0*/
La17:
        btst    #4,(a0)
        beq.b   La18              ;/* handshake */
        dbra    d1,La17
        bra     timeout_err
La18:
        move.b  (a0),d0         ;/* Y3 Y2 Y1 Y0 */
        move.b  #$60,(a0)      ;/* first phase */
        lsl.b   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 X3 X2 X1 X0 Y7 Y6 Y5 Y4 Y3 Y2 Y1 Y0 0  0  0  0 */
        lsr.l   #4,d0           ;/* YO XO YS XS S  M  R  L  X7 X6 X5 X4 X3 X2 X1 X0 Y7 Y6 Y5 Y4 Y3 Y2 Y1 Y0 */
La19:
        btst    #4,(a0)
        beq.b   La19             ;/* wait on handshake */

        ;move.w  d2,sr           ;/* restore int status */
        rts


timeout_err:
        move.b  #$60,(a0)      ;/* first phase */
        nop
        nop
La0b:
        btst    #4,(a0)
        beq.b   La0b              ;/* wait on handshake */

        ;move.w  d2,sr           ;/* restore int status */
        moveq   #-2,d0
        rts

mky_err:
        move.b  #$40,6(a0)     ;/* set direction bits */
        nop
        nop
        move.b  #$40,(a0)

        ;move.w  d2,sr           ;/* restore int status */
        moveq   #-1,d0
        rts

  ;'------------------------------------------------------------------------------------
  ;'------------------------------------------------------------------------------------
   
  