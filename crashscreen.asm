crashed:	    
        bsr setup_vdp2
		bsr clear_vram3
		lea (font),a5
		move.w #$0C00,d4
		move.l #$40000000,(a3)
		bsr load_vram2
		bsr set_cram3
		lea (crash_screen),a5
		move.w #$0700,d4
		move.l #$60000003,(a3) ;vram write $E000		
		bsr crashtxt_loop
		rts	
setup_vdp2:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray_crash).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loopcrash:
        move.w (a5)+,(a3)       ;load setup array
        dbf d4,VDP_Loopcrash
        rts  
load_vram2:
		move.w (a5)+,(a4)
		dbf d4,load_vram2
		rts
crashtxt_loop:		
		move.b (a5)+,d5
		andi.w #$00ff,d5
        move.w d5,(a4)
		dbf d4, crashtxt_loop
        rts		
set_cram3:
		move.l #$C0000000,(a3)
		move.w #$0000, (a4)
		move.w #$0000, (a4)
		move.w #$0000, (a4)
		move.w #$0EEE, (a4)
		rts
clear_vram3:				       
        move.l  #$40000000,(a3) ;set VRAM write $0000
clear_loop3:         
		nop
        move.w  #$0000,(a4)
        dbf d4,clear_loop3
        rts	
				
errordisplay:
		;move.w sr, sr_temp
		;add.l #$00000008,a7
		move.w (a7)+, sr_temp
		;sub.l #$00000008,a7				
		move.w #$2700,sr
		
		;add.l #$0000000a,a7
		move.l (a7), pc_temp 
		;sub.l #$0000000a,a7					
		move.l d0,D0_temp
		move.l d1,D1_temp
		move.l d2,D2_temp
		move.l d3,D3_temp
		move.l d4,D4_temp
		move.l d5,D5_temp
		move.l d6,D6_temp
		move.l d7,D7_temp
		move.l a0,A0_temp
		move.l a1,A1_temp
		move.l a2,A2_temp
		move.l a3,A3_temp
		move.l a4,A4_temp
		move.l a5,A5_temp
		move.l a6,A6_temp
		move.l a7,A7_temp
		bsr crashed
		move.l #$00000000,a0
		move.l #$00000000,a1
		move.l #$00000000,a2
		move.l #$00000000,a3
		move.l #$00000000,a4
		move.l #$00000000,a5
		move.l #$00000000,a6
		move.l #$00000000,d0
		move.l #$00000000,d1
		move.l #$00000000,d2
		move.l #$00000000,d3
		move.l #$00000000,d4
		move.l #$00000000,d5
		move.l #$00000000,d6
		move.l #$00000000,d7
		
        lea $C00004.l,a3      ;VDP control port
	    lea $C00000.l,a4      ;VDP data port
		
		move.w sr_temp,d0
		move.l #$6C320003,(a3);vram write $EC32	
		bsr reg_dump_word

		move.l pc_temp,d0
		move.l #$6BB20003,(a3);vram write $EBB2			
		bsr reg_dump_24bit
		
		move.l a0_temp,d0
		move.l #$688A0003,(a3);vram write $E88A
		bsr reg_dump				
		move.l a1_temp,d0
		move.l #$68A40003,(a3);vram write $E8a4
		bsr reg_dump						
		move.l a2_temp,d0
		move.l #$68BE0003,(a3);vram write $E8be
		bsr reg_dump				
		move.l a3_temp,d0
		move.l #$690a0003,(a3);vram write $E90a	
		bsr reg_dump	
		move.l a4_temp,d0
		move.l #$69240003,(a3);vram write $E924		
		bsr reg_dump	
		move.l a5_temp,d0
		move.l #$693E0003,(a3);vram write $E93E
		bsr reg_dump		
		move.l a6_temp,d0
		move.l #$698A0003,(a3);vram write $E98A
		bsr reg_dump	
		move.l a7_temp,d0
		move.l #$69A40003,(a3);vram write $E9a4
		bsr reg_dump

		move.l d0_temp,d0
		move.l #$658A0003,(a3);vram write $E58A
		bsr reg_dump
		move.l d1_temp,d0
		move.l #$65a40003,(a3);vram write $E5a4
		bsr reg_dump
		move.l d2_temp,d0
		move.l #$65BE0003,(a3);vram write $E5BE
		bsr reg_dump
		move.l d3_temp,d0
		move.l #$660A0003,(a3);vram write $E60a
		bsr reg_dump
		move.l d4_temp,d0
		move.l #$66240003,(a3);vram write $E624
		bsr reg_dump
		move.l d5_temp,d0
		move.l #$663E0003,(a3);vram write $E63D
		bsr reg_dump	
		move.l d6_temp,d0
		move.l #$668A0003,(a3);vram write $E68A
		bsr reg_dump
		move.l d7_temp,d0
		move.l #$66a40003,(a3);vram write $E6a4
		bsr reg_dump
	    bsr display_type	  ;flavor of error
	    move.l #$50000010,(a3) ;write to VSRAM
		move.w #$00f8,(a4)
		move.w #$00f8,(a4)	   ;centre screen
		bra errorhang		
reg_dump:
		clr d1
		move.l d0,d1
		andi.l #$F0000000,d1
		swap d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)	
		clr d1
		move.l d0,d1
		andi.l #$0F000000,d1
		swap d1
		lsr.w #$08,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
reg_dump_24bit:		
		clr d1
		move.l d0,d1
		andi.l #$00F00000,d1
		swap d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)				
		clr d1
		move.l d0,d1
		andi.l #$000F0000,d1
		swap d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
reg_dump_word:		
		clr d1
		move.w d0,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.w d0,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		clr d1
		move.w d0,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.w d0,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		rts
display_type:
		clr d5
		clr d0
		lea (errortable),a0     ;load the sound jump table 
		move.b errorID,d0
        lsl.b #$1,d0		    ;locate the correct sound in the table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jsr (a0)		

		move.w #$0013,d4
		move.l #$639A0003,(a3);vram write $E39A	
        bsr crashtxt_loop		
		rts
		
ErrorTrap:
        move.b #$01,errorID 
        bra errordisplay
IllegalInstr:
        move.b #$02,errorID         
        bra errordisplay
ZeroDivide:
        move.b #$03,errorID
        bra errordisplay
ChkInstr:
        move.b #$04,errorID
        bra errordisplay
TrapvInstr:
        move.b #$05,errorID	
        bra errordisplay
privilegevio:
        move.b #$06,errorID
        bra errordisplay
Trace:
        move.b #$07,errorID     
        bra errordisplay
Line1010Emu:
        move.b #$08,errorID
        bra errordisplay
Line1111Emu:
        move.b #$09,errorID
        bra errordisplay
BusError:
        move.b #$0a,errorID
        bra errordisplay	       
AddressError:
        move.b #$0b,errorID	
        bra errordisplay
errorhang:
		nop
        bra errorhang
errortable:
  dc.w ld_ErrorTrap   
  dc.w ld_IllegalInstr         
  dc.w ld_ZeroDivide
  dc.w ld_ChkInstr
  dc.w ld_TrapvInstr	
  dc.w ld_privilegevio
  dc.w ld_Trace    
  dc.w ld_Line1010Emu
  dc.w ld_Line1111Emu
  dc.w ld_BusError       
  dc.w ld_AddressError

ld_ErrorTrap:
  lea (error_text),a5
  rts
ld_IllegalInstr:
  lea (illegal_text),a5
  rts
ld_ZeroDivide:
  lea (zerodiv_text),a5 
  rts
ld_ChkInstr:
  lea (chkinstr_text),a5 
  rts
ld_TrapvInstr:	
  lea (trapv_text),a5
  rts
ld_privilegevio:
  lea (privvio_text),a5
  rts
ld_Trace:    
  lea (trace_text),a5
  rts
ld_Line1010Emu:
  lea (line1010_text),a5
  rts
ld_Line1111Emu:
  lea (line1111_text),a5 
  rts
ld_BusError:       
  lea (buserr_text),a5
  rts
ld_AddressError:	
  lea (addresserr_text),a5 
  rts
  		
			
VDPSetupArray_crash:
	dc.w $8014		
	dc.w $8164  ;Genesis mode, DMA disabled, VBLANK-INT enabled		
	dc.w $8238	;field A    
	dc.w $8300	;$833e	
	dc.w $8407	;field B	
	dc.w $857e	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81
	dc.w $8D00  ;h scroll		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
crash_screen:	
	dc.b "  Something went terribly wrong, and                            "	
	dc.b "  the game has struck a fatal error.                            "	
	dc.b " Please E-mail a screenshot of this to:                         "
	dc.b "       > ComradeOj@Yahoo.com <                                  "
	dc.b "  Also include a description of what                            "
	dc.b "  you were doing when the game crashed                          "
	dc.b "----------------------------------------------------------------"
	dc.b "  Error type:                                                   "
    dc.b "                                                                "
	dc.b "           *Data registers*                                     "	
	dc.b "                                                                "
	dc.b " D0:$???????? D1:$???????? D2:$????????                         "
	dc.b " D3:$???????? D4:$???????? D5:$????????                         "
	dc.b " D6:$???????? D7:$????????                                      "
	dc.b "                                                                "
	dc.b "          *Address registers*                                   "	
	dc.b "                                                                "
	dc.b " A0:$???????? A1:$???????? A2:$????????                         "
	dc.b " A3:$???????? A4:$???????? A5:$????????                         "
	dc.b " A6:$???????? A7:$????????                                      "
	dc.b "                                                                "
	dc.b "          *Special registers*                                   "
	dc.b "                                                                "
	dc.b "        Program counter:$??????                                 "
	dc.b "        Status register:$????                                   "
	dc.b "           Game version: 2.0                                    "
	dc.b "                                                                "
	dc.b "                                                                "
	
error_text:
 dc.b " Generic error trap " ;$14 bytes each
illegal_text:
 dc.b " Illegal instruction"
zerodiv_text 
 dc.b " Divided by zero    "
chkinstr_text: 
 dc.b " Check instruction  "
trapv_text: 
 dc.b " TrapV instruction  "
privvio_text: 
 dc.b " Privledge violation"
trace_text: 
 dc.b " Trace              "
line1010_text: 
 dc.b " Line 1010 emulator "
line1111_text: 
 dc.b " Line 1111 emulator "
buserr_text: 
 dc.b " Bus error          "
addresserr_text: 
 dc.b " Address error      "
